<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $primaryKey = 'product_id';

	protected $fillable = ['name', 'category_id', 'slug', 'weight', 'description', 'available', 'image', 'images'];

	public function category()
	{
		return $this->belongsTo('App\Category','category_id','category_id');
	}

	public function scopeAvailable($query)
	{
		return $query->where('available','1');
	}

	public function getProductBySlug($slug)
	{
		return $this->available()->where('slug',$slug)->firstOrFail();
	}

	public function getAllAvailable()
	{
		return $this->available()->orderBy('weight')->paginate(15);
	}

	public function galery()
	{
		return $this->hasMany('App\Gallery','product_id','product_id');
	}

	public function getGalery()
	{
		return $this->galery()->orderBy('weight')->get();
	}

	public function review()
	{
		return $this->hasMany('App\Review','product_id','product_id');
	}

	public function getReview()
	{
		return $this->review()->get();
	}

	public function getTopRated($count)
	{
		return $this->available()->orderBy('weight')->get()->take($count);
	}

	public function getImagesAttribute($value)
	{
			return preg_split('/,/', $value, -1, PREG_SPLIT_NO_EMPTY);
	}
	public function setImagesAttribute($images)
	{
			$this->attributes['images'] = implode(',', $images);
	}

}
