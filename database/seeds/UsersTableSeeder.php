<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([

          'name' => 'ruff0',
          'email' => 'ruff0@400tres.com',
          'password' => bcrypt('imthespi')

      ]);
      DB::table('users')->insert([

          'name' => 'ictius',
          'email' => 'ictius@kliaki.com',
          'password' => bcrypt('ictiu5')

      ]);
      DB::table('users')->insert([

          'name' => 'marcos',
          'email' => 'marcos@mail.com',
          'password' => bcrypt('corona')

      ]);
    }
}
