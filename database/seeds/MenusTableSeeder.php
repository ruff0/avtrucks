<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'name' => 'Buy',
            'url' => 'buy',
            'weight' => '1',
        ]);
        DB::table('menus')->insert([
            'name' => 'Sell',
            'url' => 'sell',
            'weight' => '2',
        ]);
        DB::table('menus')->insert([
            'name' => 'Export',
            'url' => 'export',
            'weight' => '2',
        ]);
    }
}
