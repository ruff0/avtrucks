<?php

use Illuminate\Database\Seeder;

class GalleriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galleries')->insert([
            'product_id' => '1',
            'image' => '2005freightlinercentury1.jpg',
            'weight' => '1',
        ]);
        DB::table('galleries')->insert([
            'product_id' => '1',
            'image' => '2005freightlinercentury2.jpg',
            'weight' => '2',
        ]);
    }
}
