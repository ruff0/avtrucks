<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = new \App\Category([
            'name' => 'Truck',
            'slug' => 'truck',
            'weight' => '1',
        ]);
        $categories->save();
        DB::table('categories')->insert([
            'name' => 'Dump Truck',
            'slug' => 'dumptruck',
            'weight' => '2',
        ]);
        DB::table('categories')->insert([
            'name' => 'Water Truck',
            'slug' => 'watertruck',
            'weight' => '3',
        ]);
        DB::table('categories')->insert([
          'name' => 'School Bus',
          'slug' => 'schoolbus',
          'weight' => '4',
        ]);
        DB::table('categories')->insert([
            'name' => 'Bobtail',
            'slug' => 'bobtail',
            'weight' => '5',
        ]);
        DB::table('categories')->insert([
            'name' => 'Platform Truck',
            'slug' => 'platformtruck',
            'weight' => '6',
        ]);
        DB::table('categories')->insert([
          'name' => 'Dry Box Trailer',
          'slug' => 'dryboxtrailer',
          'weight' => '7',
        ]);
        DB::table('categories')->insert([
          'name' => 'Pick Up',
          'slug' => 'pickup',
          'weight' => '8',
        ]);
        DB::table('categories')->insert([
            'name' => 'Low-bed Trailer',
            'slug' => 'lowbedtrailer',
            'weight' => '9',
        ]);



        DB::table('categories')->insert([
            'name' => 'Platform Trailer',
            'slug' => 'platformtrailer',
            'weight' => '10',
        ]);
        DB::table('categories')->insert([
            'name' => 'Refrigerated Trailer',
            'slug' => 'refrigeratedtrailer',
            'weight' => '11',
        ]);
        DB::table('categories')->insert([
            'name' => 'Generator',
            'slug' => 'generator',
            'weight' => '12',
        ]);
        DB::table('categories')->insert([
            'name' => 'Backhoe Loader',
            'slug' => 'backhoeloader',
            'weight' => '13',
        ]);
        DB::table('categories')->insert([
            'name' => 'Caterpillar',
            'slug' => 'caterpillar',
            'weight' => '14',
        ]);
        DB::table('categories')->insert([
          'name' => 'Bulldozer',
          'slug' => 'bulldozer',
          'weight' => '15',
        ]);
        DB::table('categories')->insert([
            'name' => 'Crane',
            'slug' => 'crane',
            'weight' => '16',
        ]);
        DB::table('categories')->insert([
            'name' => 'Motor Grader',
            'slug' => 'motorgrader',
            'weight' => '17',
        ]);
        DB::table('categories')->insert([
            'name' => 'Mixer',
            'slug' => 'mixer',
            'weight' => '18',
        ]);
        DB::table('categories')->insert([
            'name' => 'Excavator',
            'slug' => 'excavator',
            'weight' => '19',
        ]);
        DB::table('categories')->insert([
            'name' => 'Compactor',
            'slug' => 'compactor',
            'weight' => '20',
        ]);


        DB::table('categories')->insert([
            'name' => 'Forklift',
            'slug' => 'forklift',
            'weight' => '21',
        ]);

    }
}
