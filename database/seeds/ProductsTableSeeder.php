<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'category_id' => 1,
            'name' => '2005 Freightliner Century',
            'slug' => '2005freightlinercentury',
            'description' => 'this is the general description, more fields will be added like, vim, engine, color, etc.',
            'image' => '2005freightlinercentury.jpg',
            'weight' => 1,
            'available' => 1
        ]);

    }
}
